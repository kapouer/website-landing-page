                <!-- Footer -->
                    <footer id="footer">
                        <ul class="icons">
                            <li><a href="https://twitter.com/lemonldapng/" target="_blank" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
                            <li><a href="https://www.facebook.com/lemonldapng/" target="_blank" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
                            <li><a href="contact.html" class="icon solid fa-envelope"><span class="label"Contact</span></a></li>
                        </ul>
                        <ul class="copyright">
                            <li>&copy; LemonLDAP::NG</li>
                            <li>Hosted by <a href="https://www.worteks.com" target="_blank">Worteks</a></li>
                            <li>Forge by <a href="https://www.ow2.org" target="_blank">OW2</a></li>
                            <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                        </ul>
                    </footer>

